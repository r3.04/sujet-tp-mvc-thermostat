package fr.univlille.iut.r304.thermo.view;

public interface ITemperatureView {

	public double getDisplayedValue();

	public void incrementAction();

	public void decrementAction();

}
