package fr.univlille.iut.r304.thermo.model;

public interface ITemperature {

	public void setTemperature(double d);
	public Double getTemperature();

	public void incrementTemperature();
	public void decrementTemperature();

}
