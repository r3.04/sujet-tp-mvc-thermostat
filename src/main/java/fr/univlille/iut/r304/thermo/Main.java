package fr.univlille.iut.r304.thermo;

import fr.univlille.iut.r304.thermo.model.Thermostat;
import fr.univlille.iut.r304.thermo.view.TextView;
import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		Thermostat thermo = new Thermostat();
		new TextView(thermo);
	}

}
