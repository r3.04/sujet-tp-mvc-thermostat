# TP4 : MVC avec le Thermostat

Dans ce TP vous allez approfondir votre compréhension du patron MVC (Modèle-Vue-Contrôleur) en implémentant ce patron sur une application très simple : un thermostat.

Ce patron se basant sur la notion d’Observateur/Observé, les classes du TP précédent sont disponibles dans le paquetage <code>fr.univlille.iut.r304.utils</code> (ça peut au passage faire office de correction).

```mermaid
classDiagram
  direction LR
  
  class Observable
  <<Abstract>> Observable
  Observable  --> "*" Observer : #observers
  Observable ..> Observer : notifies
  Observable : +attach(Observer) void
  Observable : +detach(Observer) void
  Observable : #notifyObservers() void
  Observable : #notifyObservers(Object) void

  class Observer
  <<interface>> Observer
  Observer : +update(Observable) void
  Observer : +update(Observable, Object) void
```

Ce TP sera aussi l’occasion de réviser vos connaissances en JavaFX.
Un *pom.xml* est fourni afin de vous faciliter la gestion des dépendances à JavaFX.
Pour commencer, cloner ce projet, éventuellement en faisant un fork préalable.
Pour vous assurer que tout fonctionne correctement, vous pouvez lancer l'application qui devrait pour le moment afficher une fenêtre vide.

Des tests sont disponibles pour valider, au moins partiellement, vos implémentations au fur et à mesure du TP.
Ils vont de pair avec l'énoncé pour votre bonne progression dans la réaliation de ce TP.
La contrepartie est de suivre les différentes étapes du TP en complétant bien les classes fournies au fur et à mesure.

## Un modèle, des vues

Un thermostat dans sa plus simple expression est un appareil ayant une température désirée et régulant le chauffage pour que la température réelle soit le plus proche possible de la température désirée.
On ne s’occupera bien sûr pas des capteurs et de la température réelle ici, ni du chauffage, mais seulement de pouvoir régler la température désirée.

On a donc un modèle et un affichage (vue + contrôleur) très simple.

**Q1** Quel affichage est nécessaire pour seulement régler la température désirée ?

**Q2** Que devrait contenir le modèle de l’application ?

Pour rappel, le modèle :
- Est indépendant de la (des) vue(s). Par exemple, pas de « import javafx » ;
- contient les données nécessaires à l’application ;
- contient aussi des comportements possibles sur ces données (mais sans s’occuper de l’affichage)

Donc quelle(s) donnée(s) doit contenir ce modèle ? Quel(s) comportement(s) ?


**Q3** Faites une première version de l’application avec un modèle et un affichage simple.

On doit pouvoir voir la valeur désirée du thermostat, changer cette valeur par une autre, l’incrémenter ou la décrémenter de 1 degré à la fois.

*Optionnel* : Permettre aussi l’incrément/décrément de 5 en 5 (avec une interaction adaptée).

S’agissant d’une implémentation MVC, les communications du modèle vers la vue (l’affichage) doivent se faire suivant le patron observateur/observé (ici disponible dans <code>fr.univlille.iut.r304.utils</code>. ).

Qui est ici l’observateur ? Qui est l’observé ?

Note : Puisque vous allez faire plusieurs vues (fenêtres) dans ce TP, il est plus pratique de ne pas utiliser le <code>PrimaryStage</code> que votre classe principale (qui hérite de Application) vous fournit dans la méthode <code>public void start(Stage primaryStage)</code>.
Il sera plus naturel que chaque vue créée sa propre instance de stage. 
**Votre classe Main ne devrait donc que très peu évoluer, simplement une ligne de code supplémentaire pour chaque nouvelle vue**

**Q4** Avec une seule vue, ce patron est inutilement compliqué.
Mais si on a plusieurs vues (par exemple un thermostat à deux extrémités d’une grande pièce), l’utilisation de Observateur/Observé prend son sens.

Rajoutez maintenant une nouvelle vue (ou 2, ou 3) en créant simplement des nouveaux objets de la classe vue (une ligne Java).
Vérifiez que toutes les vues sont toujours synchronisées (affichent la même température désirée) grâce au MVC (et aux observateurs/observé).
On doit pouvoir aussi modifier la température en la saisissant directement dans le champs texte (d'une des vues). Dans ce cas aussi, toutes les vues doivent se mettre à jour.

Créez une nouvelle vue utilisant un Slider pour afficher et modifier la température désirée.
On peut décider que les températures sont bornées entre -10.0 ⁰C et +30.0⁰C.
Bien sûr, les anciennes vues doivent continuer à fonctionner.

![Exemple de plusieurs vues synchornisées sur un modèle](documentation/multipleViews.gif)
